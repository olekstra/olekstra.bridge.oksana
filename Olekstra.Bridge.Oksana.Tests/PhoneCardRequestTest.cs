namespace Olekstra.Bridge.Oksana
{
    using System;
    using Xunit;

    public class PhoneCardRequestTest
    {
        public const string ValidPhoneNumber = OrderUpdateTest.ValidPhoneNumber;

        [Fact]
        public void TesSerializeAndDeserializeFull()
        {
            var srcObj = new PhoneCardRequest
            {
                Phone = ValidPhoneNumber,
            };

            var srcText = SerializationHelper.SerializeXml(srcObj);

            Assert.NotNull(srcText);

            var newObj = SerializationHelper.DeserializeXml<PhoneCardRequest>(srcText);

            Assert.NotNull(newObj);
            Assert.Equal(srcObj.Phone, newObj.Phone);
        }

        [Fact]
        public void SampleDeserialize()
        {
            var newObj = SerializationHelper.DeserializeXml<PhoneCardRequest>(Messages.PhoneCardRequest);

            Assert.NotNull(newObj);
            Assert.Equal("+79000000000", newObj.Phone);
        }
    }
}
