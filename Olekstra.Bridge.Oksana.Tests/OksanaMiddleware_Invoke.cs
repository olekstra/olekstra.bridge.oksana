﻿namespace Olekstra.Bridge.Oksana
{
    using System;
    using System.IO;
    using System.Net;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Http;
    using Microsoft.Extensions.Logging;
    using Moq;
    using Xunit;

    public class OksanaMiddleware_Invoke
    {
        [Theory]
        [InlineData(true, HttpStatusCode.BadRequest)]
        [InlineData(false, HttpStatusCode.OK)]
        public async Task ProcessCardStatusRequest(bool throwArgumentException, HttpStatusCode expectedStatusCode)
        {
            var fakeContext = new DefaultHttpContext();

            using (var ms = new MemoryStream())
            {
                using (var sw = new StreamWriter(ms, Encoding.UTF8, 2048, true))
                {
                    sw.Write(Messages.CardStatusRequest);
                }

                ms.Position = 0;

                fakeContext.Request.Path = "/cardStatus";
                fakeContext.Request.Headers["Authorization"] = "Basic " + OksanaMiddleware_AuthorizeAsync.Base64ValidUserPass;
                fakeContext.Request.ContentType = "application/xml";
                fakeContext.Request.Method = "POST";
                fakeContext.Request.Body = ms;

                var serviceMock = new Mock<IOksanaService>(MockBehavior.Strict);

                serviceMock.Setup(x => x.AuthorizeAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(true);

                if (throwArgumentException)
                {
                    serviceMock
                        .Setup(x => x.ProcessCardStatusRequestAsync(It.IsAny<string>(), It.IsAny<CardStatusRequest>()))
                        .ThrowsAsync(new ArgumentException("Test message"));
                }
                else
                {
                    serviceMock
                        .Setup(x => x.ProcessCardStatusRequestAsync(It.IsAny<string>(), It.IsAny<CardStatusRequest>()))
                        .ReturnsAsync(new CardStatus { Number = "6812345678901", Status = true });
                }

                var middleware = new OksanaMiddleware(
                    null,
                    serviceMock.Object,
                    new Mock<ILogger<OksanaMiddleware>>().Object);

                await middleware.Invoke(fakeContext);
            }

            Assert.Equal((int)expectedStatusCode, fakeContext.Response.StatusCode);
        }

        [Theory]
        [InlineData(true, HttpStatusCode.BadRequest)]
        [InlineData(false, HttpStatusCode.OK)]
        public async Task ProcessPhoneCardRequest(bool throwArgumentException, HttpStatusCode expectedStatusCode)
        {
            var fakeContext = new DefaultHttpContext();

            using (var ms = new MemoryStream())
            {
                using (var sw = new StreamWriter(ms, Encoding.UTF8, 2048, true))
                {
                    sw.Write(Messages.PhoneCardRequest);
                }

                ms.Position = 0;

                fakeContext.Request.Path = "/phoneCard";
                fakeContext.Request.Headers["Authorization"] = "Basic " + OksanaMiddleware_AuthorizeAsync.Base64ValidUserPass;
                fakeContext.Request.ContentType = "application/xml";
                fakeContext.Request.Method = "POST";
                fakeContext.Request.Body = ms;

                var serviceMock = new Mock<IOksanaService>(MockBehavior.Strict);

                serviceMock.Setup(x => x.AuthorizeAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(true);

                if (throwArgumentException)
                {
                    serviceMock
                        .Setup(x => x.ProcessPhoneCardRequestAsync(It.IsAny<string>(), It.IsAny<PhoneCardRequest>()))
                        .ThrowsAsync(new ArgumentException("Test message"));
                }
                else
                {
                    serviceMock
                        .Setup(x => x.ProcessPhoneCardRequestAsync(It.IsAny<string>(), It.IsAny<PhoneCardRequest>()))
                        .ReturnsAsync(new PhoneCard { Phone = "+79000000000", Card = "6812345678901", Status = true });
                }

                var middleware = new OksanaMiddleware(
                    null,
                    serviceMock.Object,
                    new Mock<ILogger<OksanaMiddleware>>().Object);

                await middleware.Invoke(fakeContext);
            }

            Assert.Equal((int)expectedStatusCode, fakeContext.Response.StatusCode);
        }

        [Theory]
        [InlineData(true, HttpStatusCode.BadRequest)]
        [InlineData(false, HttpStatusCode.OK)]
        public async Task ProcessOrderUpdate(bool throwArgumentException, HttpStatusCode expectedStatusCode)
        {
            var fakeContext = new DefaultHttpContext();

            using (var ms = new MemoryStream())
            {
                using (var sw = new StreamWriter(ms, Encoding.UTF8, 2048, true))
                {
                    sw.Write(Messages.OrderUpdate);
                }

                ms.Position = 0;

                fakeContext.Request.Path = "/orderUpdate";
                fakeContext.Request.Headers["Authorization"] = "Basic " + OksanaMiddleware_AuthorizeAsync.Base64ValidUserPass;
                fakeContext.Request.ContentType = "application/xml";
                fakeContext.Request.Method = "POST";
                fakeContext.Request.Body = ms;

                var serviceMock = new Mock<IOksanaService>(MockBehavior.Strict);

                serviceMock.Setup(x => x.AuthorizeAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(true);

                if (throwArgumentException)
                {
                    serviceMock
                        .Setup(x => x.ProcessOrderUpdateAsync(It.IsAny<string>(), It.IsAny<OrderUpdate>()))
                        .ThrowsAsync(new ArgumentException("Test message"));
                }
                else
                {
                    serviceMock
                        .Setup(x => x.ProcessOrderUpdateAsync(It.IsAny<string>(), It.IsAny<OrderUpdate>()))
                        .Returns(Task.CompletedTask);
                }

                var middleware = new OksanaMiddleware(
                    null,
                    serviceMock.Object,
                    new Mock<ILogger<OksanaMiddleware>>().Object);

                await middleware.Invoke(fakeContext);
            }

            Assert.Equal((int)expectedStatusCode, fakeContext.Response.StatusCode);
        }
    }
}
