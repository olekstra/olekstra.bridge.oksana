namespace Olekstra.Bridge.Oksana
{
    using System;
    using Xunit;

    public class OrderUpdateResultTest
    {
        [Fact]
        public void TesSerializeAndDeserializeFull()
        {
            var srcObj = new OrderUpdateResult
            {
                Status = true,
            };

            var srcText = SerializationHelper.SerializeXml(srcObj);

            Assert.NotNull(srcText);

            var newObj = SerializationHelper.DeserializeXml<OrderUpdateResult>(srcText);

            Assert.NotNull(newObj);
            Assert.Equal(srcObj.Status, newObj.Status);
            Assert.Equal(srcObj.StatusText, newObj.StatusText);
        }

        [Fact]
        public void SampleDeserialize()
        {
            var newObj = SerializationHelper.DeserializeXml<OrderUpdateResult>(Messages.OrderUpdateResult);

            Assert.NotNull(newObj);
            Assert.True(newObj.Status);
        }
    }
}
