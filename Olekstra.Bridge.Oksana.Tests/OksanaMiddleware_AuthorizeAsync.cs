﻿namespace Olekstra.Bridge.Oksana
{
    using System;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Http;
    using Microsoft.Extensions.Logging;
    using Moq;
    using Xunit;

    public class OksanaMiddleware_AuthorizeAsync
    {
        public const string Base64ValidUserPass = "QWxhZGRpbjpvcGVuIHNlc2FtZQ==";

        private const string ValidUsername = "Aladdin";

        private const string ValidPassword = "open sesame";

        [Fact]
        public async Task FailOnNoHeader()
        {
            var fakeContext = new DefaultHttpContext();

            var middleware = new OksanaMiddleware(
                null,
                new Mock<IOksanaService>().Object,
                new Mock<ILogger<OksanaMiddleware>>().Object);

            var result = await middleware.AuthorizeAsync(fakeContext.Request);

            Assert.False(result.success);
        }

        [Fact]
        public async Task FailOnWrongHeader()
        {
            var fakeContext = new DefaultHttpContext();
            fakeContext.Request.Headers["Authorization"] = "Uknown 123";

            var middleware = new OksanaMiddleware(
                null,
                new Mock<IOksanaService>().Object,
                new Mock<ILogger<OksanaMiddleware>>().Object);

            var result = await middleware.AuthorizeAsync(fakeContext.Request);

            Assert.False(result.success);
        }

        [Theory]
        [InlineData(ValidUsername, ValidPassword, true)]
        [InlineData(ValidUsername, "wrong password", false)]
        [InlineData("wrong username", ValidPassword, false)]
        public async Task TrustServiceResponse(string username, string password, bool authorized)
        {
            var fakeContext = new DefaultHttpContext();
            fakeContext.Request.Headers["Authorization"] = "Basic " + Base64ValidUserPass;

            var serviceMock = new Mock<IOksanaService>(MockBehavior.Strict);
            serviceMock
                .Setup(x => x.AuthorizeAsync(It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync((string u, string p) => u == username && p == password);

            var middleware = new OksanaMiddleware(
                null,
                serviceMock.Object,
                new Mock<ILogger<OksanaMiddleware>>().Object);

            var result = await middleware.AuthorizeAsync(fakeContext.Request);

            Assert.Equal(authorized, result.success);

            if (authorized)
            {
                Assert.Equal(username, result.username);
            }
        }
    }
}
