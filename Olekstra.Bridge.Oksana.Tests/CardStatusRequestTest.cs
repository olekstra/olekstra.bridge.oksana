namespace Olekstra.Bridge.Oksana
{
    using System;
    using Xunit;

    public class CardStatusRequestTest
    {
        public const string ValidCardNumber = "6801234567890";

        [Fact]
        public void TesSerializeAndDeserializeFull()
        {
            var srcObj = new CardStatusRequest
            {
                Number = ValidCardNumber,
            };

            var srcText = SerializationHelper.SerializeXml(srcObj);

            Assert.NotNull(srcText);

            var newObj = SerializationHelper.DeserializeXml<CardStatusRequest>(srcText);

            Assert.NotNull(newObj);
            Assert.Equal(srcObj.Number, newObj.Number);
        }

        [Fact]
        public void SampleDeserialize()
        {
            var newObj = SerializationHelper.DeserializeXml<CardStatusRequest>(Messages.CardStatusRequest);

            Assert.NotNull(newObj);
            Assert.Equal("6800000000001", newObj.Number);
        }
    }
}
