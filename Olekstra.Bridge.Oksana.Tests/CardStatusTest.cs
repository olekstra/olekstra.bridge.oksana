﻿namespace Olekstra.Bridge.Oksana
{
    using System;
    using Xunit;

    public class CardStatusTest
    {
        public const string ValidCardNumber = CardStatusRequestTest.ValidCardNumber;

        [Fact]
        public void TesSerializeAndDeserializeFull()
        {
            var srcObj = new CardStatus
            {
                Number = ValidCardNumber,
                Status = true,
                StatusText = "Some status",
                DiscountPlanId = "TESTPLAN",
            };

            var srcText = SerializationHelper.SerializeXml(srcObj);

            Assert.NotNull(srcText);

            var newObj = SerializationHelper.DeserializeXml<CardStatus>(srcText);

            Assert.NotNull(newObj);
            Assert.Equal(srcObj.Number, newObj.Number);
            Assert.Equal(srcObj.Status, newObj.Status);
            Assert.Equal(srcObj.StatusText, newObj.StatusText);
            Assert.Equal(srcObj.DiscountPlanId, newObj.DiscountPlanId);
        }

        [Fact]
        public void SampleDeserializePositive()
        {
            var newObj = SerializationHelper.DeserializeXml<CardStatus>(Messages.CardStatusPositive);

            Assert.NotNull(newObj);
            Assert.Equal("6800000000001", newObj.Number);
            Assert.True(newObj.Status);
            Assert.Equal("TEST_PLAN", newObj.DiscountPlanId);
        }

        [Fact]
        public void SampleDeserializeNegative()
        {
            var newObj = SerializationHelper.DeserializeXml<CardStatus>(Messages.CardStatusNegative);

            Assert.NotNull(newObj);
            Assert.Equal("6800000000001", newObj.Number);
            Assert.False(newObj.Status);
            Assert.Equal("Card comment", newObj.StatusText);
        }
    }
}
