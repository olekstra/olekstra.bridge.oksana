﻿namespace Olekstra.Bridge.Oksana
{
    using System;
    using Xunit;

    public class OrderUpdateTest
    {
        public const string ValidCardNumber = CardStatusRequestTest.ValidCardNumber;

        public const string ValidPhoneNumber = "+79001234567";

        [Fact]
        public void TestSerializeAndDeserializeFull()
        {
            var srcObj = new OrderUpdate
            {
                OrderId = "ID123",
                Created = DateTimeOffset.Now.AddHours(-2),
                Updated = DateTimeOffset.Now.AddHours(-1),
                CardNumber = ValidCardNumber,
                PhoneNumber = ValidPhoneNumber,
                Rows = new[]
                {
                    new OrderUpdate.Row
                    {
                        RowId = "R1",
                        SkuId = "SKU1",
                        SkuName = "SKU One",
                        Preorder = false,
                        Quantity = 2,
                        Price = 1234.56M,
                        DiscountPerItem = 134.50M,
                        SumToPay = 2200.12M,
                        Status = OrderUpdateRowStatus.Cancelled,
                        StatusText = "Cancelled as Text",
                    },
                    new OrderUpdate.Row
                    {
                        RowId = "R2",
                        SkuId = "SKU2",
                        SkuName = "SKU Two",
                        Preorder = true,
                        Quantity = 1,
                        Price = 1234.56M,
                        DiscountPerItem = 134.50M,
                        SumToPay = 1100.06M,
                        Status = OrderUpdateRowStatus.Purchased,
                        StatusText = "Purchased as Text",
                        Purchased = DateTimeOffset.Now.AddMinutes(-1),
                    },
                },
                DeliveryPoint = new OrderUpdate.DeliveryPointInfo
                {
                    Id = "Some ID",
                    Name = "Sample name",
                    Address = "Somewhere here...",
                    Latitude = 123.45F,
                    Longitude = 567.89F,
                    OpenHours = "All day",
                    Phones = "3 iPhones and 1 Android",
                },
            };

            var srcText = SerializationHelper.SerializeXml(srcObj);

            Assert.NotNull(srcText);

            var newObj = SerializationHelper.DeserializeXml<OrderUpdate>(srcText);

            Assert.NotNull(newObj);
            Assert.Equal(srcObj.OrderId, newObj.OrderId);
            Assert.Equal(srcObj.Created, newObj.Created);
            Assert.Equal(srcObj.Updated, newObj.Updated);
            Assert.Equal(srcObj.CardNumber, newObj.CardNumber);
            Assert.Equal(srcObj.PhoneNumber, newObj.PhoneNumber);
            Assert.Equal(srcObj.Rows.Length, newObj.Rows.Length);

            for (var i = 0; i < srcObj.Rows.Length; i++)
            {
                Assert.Equal(srcObj.Rows[i].RowId, newObj.Rows[i].RowId);
                Assert.Equal(srcObj.Rows[i].SkuId, newObj.Rows[i].SkuId);
                Assert.Equal(srcObj.Rows[i].SkuName, newObj.Rows[i].SkuName);
                Assert.Equal(srcObj.Rows[i].Preorder, newObj.Rows[i].Preorder);
                Assert.Equal(srcObj.Rows[i].Quantity, newObj.Rows[i].Quantity);
                Assert.Equal(srcObj.Rows[i].Price, newObj.Rows[i].Price);
                Assert.Equal(srcObj.Rows[i].DiscountPerItem, newObj.Rows[i].DiscountPerItem);
                Assert.Equal(srcObj.Rows[i].SumToPay, newObj.Rows[i].SumToPay);
                Assert.Equal(srcObj.Rows[i].Status, newObj.Rows[i].Status);
                Assert.Equal(srcObj.Rows[i].StatusText, newObj.Rows[i].StatusText);
                Assert.Equal(srcObj.Rows[i].Purchased, newObj.Rows[i].Purchased);
            }

            Assert.NotNull(newObj.DeliveryPoint);
            Assert.Equal(srcObj.DeliveryPoint.Id, newObj.DeliveryPoint.Id);
            Assert.Equal(srcObj.DeliveryPoint.Name, newObj.DeliveryPoint.Name);
            Assert.Equal(srcObj.DeliveryPoint.Address, newObj.DeliveryPoint.Address);
            Assert.Equal(srcObj.DeliveryPoint.Latitude, newObj.DeliveryPoint.Latitude);
            Assert.Equal(srcObj.DeliveryPoint.Longitude, newObj.DeliveryPoint.Longitude);
            Assert.Equal(srcObj.DeliveryPoint.OpenHours, newObj.DeliveryPoint.OpenHours);
            Assert.Equal(srcObj.DeliveryPoint.Phones, newObj.DeliveryPoint.Phones);
        }

        [Fact]
        public void SampleDeserialize()
        {
            var additionalValidationFunc = new Action<OrderUpdate>(obj =>
            {
                Assert.Null(obj.DeliveryPoint);
            });

            SampleDeserializeInternal(Messages.OrderUpdate, additionalValidationFunc);
        }

        [Fact]
        public void SampleDeserialize2()
        {
            var additionalValidationFunc = new Action<OrderUpdate>(obj =>
            {
                Assert.NotNull(obj.DeliveryPoint);
                Assert.Equal("A23", obj.DeliveryPoint.Id);
                Assert.Equal("Some name", obj.DeliveryPoint.Name);
                Assert.Equal("Central park", obj.DeliveryPoint.Address);
                Assert.Equal(123.45F, obj.DeliveryPoint.Latitude);
                Assert.Equal(678.90F, obj.DeliveryPoint.Longitude);
                Assert.Equal("8-10 AM", obj.DeliveryPoint.OpenHours);
                Assert.Equal("None", obj.DeliveryPoint.Phones);
            });

            SampleDeserializeInternal(Messages.OrderUpdateWithDeliveryPoint, additionalValidationFunc);
        }

        [Fact]
        public void SampleDeserialize3()
        {
            var additionalValidationFunc = new Action<OrderUpdate>(obj =>
            {
                Assert.NotNull(obj.DeliveryPoint);
                Assert.Equal("A23", obj.DeliveryPoint.Id);
                Assert.Equal("Some name", obj.DeliveryPoint.Name);
                Assert.Equal("My Region", obj.DeliveryPoint.Region);
                Assert.Equal("Central park", obj.DeliveryPoint.Address);
                Assert.Equal(123.45F, obj.DeliveryPoint.Latitude);
                Assert.Equal(678.90F, obj.DeliveryPoint.Longitude);
                Assert.Equal("8-10 AM", obj.DeliveryPoint.OpenHours);
                Assert.Equal("None", obj.DeliveryPoint.Phones);
            });

            SampleDeserializeInternal(Messages.OrderUpdateWithDeliveryPointRegion, additionalValidationFunc);
        }

        private void SampleDeserializeInternal(string sample, Action<OrderUpdate> additionalValidation)
        {
            var newObj = SerializationHelper.DeserializeXml<OrderUpdate>(sample);

            Assert.NotNull(newObj);

            Assert.Equal("A-123", newObj.OrderId);
            Assert.Equal(new DateTimeOffset(2019, 1, 2, 17, 18, 19, 0, TimeSpan.FromHours(3)), newObj.Created);
            Assert.Equal(new DateTimeOffset(2019, 1, 3, 17, 19, 20, 0, TimeSpan.FromHours(2)), newObj.Updated);
            Assert.Equal("6800000000001", newObj.CardNumber);
            Assert.Equal("+79001234567", newObj.PhoneNumber);
            Assert.Equal(2, newObj.Rows.Length);

            var row = newObj.Rows[0];
            Assert.Equal("A-123-1", row.RowId);
            Assert.Equal("SKU123", row.SkuId);
            Assert.Equal("SKU 123 name", row.SkuName);
            Assert.True(row.Preorder);
            Assert.Equal(2, row.Quantity);
            Assert.Equal(123.45M, row.Price);
            Assert.Equal(23.45M, row.DiscountPerItem);
            Assert.Equal(100M, row.SumToPay);
            Assert.Equal(OrderUpdateRowStatus.Processing, row.Status);
            Assert.Equal("Processing status text", row.StatusText);
            Assert.Equal(new DateTimeOffset(2019, 1, 4, 11, 12, 13, 0, TimeSpan.FromHours(3)), row.Purchased);

            row = newObj.Rows[1];
            Assert.Equal("A-123-2", row.RowId);
            Assert.Equal("SKU123", row.SkuId);
            Assert.Equal("SKU 123 name", row.SkuName);
            Assert.False(row.Preorder);
            Assert.Equal(2, row.Quantity);
            Assert.Equal(123.45M, row.Price);
            Assert.Equal(23.45M, row.DiscountPerItem);
            Assert.Equal(100M, row.SumToPay);
            Assert.Equal(OrderUpdateRowStatus.Processing, row.Status);
            Assert.Equal("Processing status text", row.StatusText);
            Assert.Null(row.Purchased);

            additionalValidation(newObj);
        }
    }
}
