﻿namespace Olekstra.Bridge.Oksana
{
    using System;
    using Xunit;

    public class PhoneCardTest
    {
        public const string ValidCardNumber = OrderUpdateTest.ValidCardNumber;
        public const string ValidPhoneNumber = OrderUpdateTest.ValidPhoneNumber;

        [Fact]
        public void TesSerializeAndDeserializeFull()
        {
            var srcObj = new PhoneCard
            {
                Phone = ValidPhoneNumber,
                Card = ValidCardNumber,
                Status = true,
                StatusText = "Some status",
                DiscountPlanId = "TEST_PLAN",
            };

            var srcText = SerializationHelper.SerializeXml(srcObj);

            Assert.NotNull(srcText);

            var newObj = SerializationHelper.DeserializeXml<PhoneCard>(srcText);

            Assert.NotNull(newObj);
            Assert.Equal(srcObj.Phone, newObj.Phone);
            Assert.Equal(srcObj.Card, newObj.Card);
            Assert.Equal(srcObj.Status, newObj.Status);
            Assert.Equal(srcObj.StatusText, newObj.StatusText);
            Assert.Equal(srcObj.DiscountPlanId, newObj.DiscountPlanId);
        }

        [Fact]
        public void SampleDeserialize()
        {
            var newObj = SerializationHelper.DeserializeXml<PhoneCard>(Messages.PhoneCard);

            Assert.NotNull(newObj);
            Assert.Equal("+79000000000", newObj.Phone);
            Assert.True(newObj.Status);
            Assert.Equal("Success", newObj.StatusText);
            Assert.Equal("6800000000001", newObj.Card);
            Assert.Equal("TEST_PLAN", newObj.DiscountPlanId);
        }
    }
}
