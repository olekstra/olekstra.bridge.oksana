﻿namespace Olekstra.Bridge.Oksana
{
    using System;

    public class OrderUpdateValidationException : Exception
    {
        public OrderUpdateValidationException()
            : base()
        {
            // Nothing
        }

        public OrderUpdateValidationException(string message)
            : base(message)
        {
            // Nothing
        }

        public OrderUpdateValidationException(string message, Exception innerException)
            : base(message, innerException)
        {
            // Nothing
        }
    }
}
