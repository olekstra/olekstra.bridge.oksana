﻿namespace Olekstra.Bridge.Oksana
{
    using System;

    public static class OrderUpdateExtensions
    {
        /// <summary>
        /// Validates internal state of order (for DateTimeOffset.Now).
        /// </summary>
        /// <param name="orderUpdate">OrderUpdate to validate.</param>
        /// <exception cref="ArgumentNullException">orderUpdate is null.</exception>
        /// <exception cref="OrderUpdateValidationException">When some order data/fields are invalid.</exception>
        public static void Validate(this OrderUpdate orderUpdate)
        {
            Validate(orderUpdate, DateTimeOffset.Now);
        }

        /// <summary>
        /// Validates internal state of order.
        /// </summary>
        /// <param name="orderUpdate">OrderUpdate to validate.</param>
        /// <param name="dateTime">Date and Time used for validation (some checks require "current" date and time).</param>
        /// <exception cref="ArgumentNullException">orderUpdate is null.</exception>
        /// <exception cref="OrderUpdateValidationException">When some order data/fields are invalid.</exception>
        public static void Validate(this OrderUpdate orderUpdate, DateTimeOffset dateTime)
        {
            if (orderUpdate == null)
            {
                throw new ArgumentNullException(nameof(orderUpdate));
            }

            if (orderUpdate.Created > dateTime)
            {
                throw new OrderUpdateValidationException("Order 'created' can't be in future");
            }

            if (orderUpdate.Updated > dateTime)
            {
                throw new OrderUpdateValidationException("Order 'updated' can't be in future");
            }

            if (orderUpdate.Created > orderUpdate.Updated)
            {
                throw new OrderUpdateValidationException("Order can't be 'updated' before 'created'");
            }

            if (orderUpdate.Rows != null)
            {
                foreach (var row in orderUpdate.Rows)
                {
                    if (row.Quantity <= 0)
                    {
                        throw new OrderUpdateValidationException($"'quantity' must be positive (in row '{row.RowId}')");
                    }

                    if (row.Price <= 0)
                    {
                        throw new OrderUpdateValidationException($"'price' must be positive (in row '{row.RowId}')");
                    }

                    if (row.DiscountPerItem < 0)
                    {
                        throw new OrderUpdateValidationException($"'discountPerItem' must be 0 or positive (in row '{row.RowId}')");
                    }

                    if (row.SumToPay < 0)
                    {
                        throw new OrderUpdateValidationException($"'sumToPay' must be 0 or positive (in row '{row.RowId}')");
                    }

                    if (row.Purchased != null)
                    {
                        if (row.Purchased < orderUpdate.Created)
                        {
                            throw new OrderUpdateValidationException($"Row '{row.RowId}' can't be 'purchased' before order 'created'");
                        }

                        if (row.Purchased > dateTime)
                        {
                            throw new OrderUpdateValidationException($"Row '{row.RowId}' can't be 'purchased' in future");
                        }
                    }
                }
            }
        }
    }
}
