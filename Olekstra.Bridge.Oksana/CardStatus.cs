﻿namespace Olekstra.Bridge.Oksana
{
    using System;
    using System.Xml.Serialization;

    [System.CodeDom.Compiler.GeneratedCode("xsd", "4.6.1055.0")]
    [System.Serializable()]
    [System.Diagnostics.DebuggerStepThrough()]
    [System.ComponentModel.DesignerCategory("code")]
    [XmlType(AnonymousType = true, Namespace = "http://www.olekstra.ru/schema/oksana")]
    [XmlRoot("cardStatus", Namespace = "http://www.olekstra.ru/schema/oksana", IsNullable = false)]
    public class CardStatus
    {
        [XmlElement("status")]
        public bool Status { get; set; }

        [XmlElement("statusText")]
        public string StatusText { get; set; }

        [XmlElement("discountPlanId")]
        public string DiscountPlanId { get; set; }

        [XmlAttribute("number")]
        public string Number { get; set; }
    }
}
