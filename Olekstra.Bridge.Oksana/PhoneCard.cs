﻿namespace Olekstra.Bridge.Oksana
{
    using System;
    using System.Xml.Serialization;

    [System.CodeDom.Compiler.GeneratedCode("xsd", "4.6.1055.0")]
    [System.Serializable()]
    [System.Diagnostics.DebuggerStepThrough()]
    [System.ComponentModel.DesignerCategory("code")]
    [XmlType(AnonymousType = true, Namespace = "http://www.olekstra.ru/schema/oksana")]
    [XmlRoot("phoneCard", Namespace = "http://www.olekstra.ru/schema/oksana", IsNullable = false)]
    public class PhoneCard
    {
        [XmlElement("status")]
        public bool Status { get; set; }

        [XmlElement("statusText")]
        public string StatusText { get; set; }

        [XmlElement("card")]
        public string Card { get; set; }

        [XmlElement("discountPlanId")]
        public string DiscountPlanId { get; set; }

        [XmlAttribute("phone")]
        public string Phone { get; set; }
    }
}
