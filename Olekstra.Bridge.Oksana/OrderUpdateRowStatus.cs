﻿namespace Olekstra.Bridge.Oksana
{
    using System.Xml.Serialization;

    public enum OrderUpdateRowStatus
    {
        [XmlEnum(Name = "processing")]
        Processing,

        [XmlEnum(Name = "preparing")]
        Preparing,

        [XmlEnum(Name = "ready")]
        Ready,

        [XmlEnum(Name = "purchased")]
        Purchased,

        [XmlEnum(Name = "cancelled")]
        Cancelled,
    }
}
