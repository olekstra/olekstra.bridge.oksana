﻿namespace Olekstra.Bridge.Oksana
{
    using System;
    using System.Xml.Serialization;

    [System.CodeDom.Compiler.GeneratedCode("xsd", "4.6.1055.0")]
    [System.Serializable()]
    [System.Diagnostics.DebuggerStepThrough()]
    [System.ComponentModel.DesignerCategory("code")]
    [XmlType(AnonymousType = true, Namespace = "http://www.olekstra.ru/schema/oksana")]
    [XmlRoot("phoneCardRequest", Namespace = "http://www.olekstra.ru/schema/oksana", IsNullable = false)]
    public class PhoneCardRequest
    {
        [XmlAttribute("phone")]
        public string Phone { get; set; }
    }
}
