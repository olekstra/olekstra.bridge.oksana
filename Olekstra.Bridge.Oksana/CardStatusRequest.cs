﻿namespace Olekstra.Bridge.Oksana
{
    using System;
    using System.Xml.Serialization;

    [System.CodeDom.Compiler.GeneratedCode("xsd", "4.6.1055.0")]
    [System.Serializable()]
    [System.Diagnostics.DebuggerStepThrough()]
    [XmlType(AnonymousType = true, Namespace = "http://www.olekstra.ru/schema/oksana")]
    [XmlRoot("cardStatusRequest", Namespace = "http://www.olekstra.ru/schema/oksana", IsNullable = false)]
    public class CardStatusRequest
    {
        [XmlAttribute("number")]
        public string Number { get; set; }
    }
}
