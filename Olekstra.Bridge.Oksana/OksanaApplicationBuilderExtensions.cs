﻿namespace Microsoft.AspNetCore.Builder
{
    using System;
    using Microsoft.AspNetCore.Http;
    using Olekstra.Bridge.Oksana;

    public static class OksanaApplicationBuilderExtensions
    {
        public static IApplicationBuilder UseOksanaBridge(this IApplicationBuilder app, PathString mappingPath)
        {
            if (app == null)
            {
                throw new ArgumentNullException(nameof(app));
            }

            return app.Map(mappingPath, b => b.UseMiddleware<OksanaMiddleware>());
        }
    }
}
