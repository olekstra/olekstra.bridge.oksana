﻿namespace Olekstra.Bridge.Oksana
{
    using System;
    using System.Xml.Serialization;

    [System.CodeDom.Compiler.GeneratedCode("xsd", "4.6.1055.0")]
    [System.Serializable()]
    [System.Diagnostics.DebuggerStepThrough()]
    [System.ComponentModel.DesignerCategory("code")]
    [XmlType(AnonymousType = true, Namespace = "http://www.olekstra.ru/schema/oksana")]
    [XmlRoot("orderUpdate", Namespace = "http://www.olekstra.ru/schema/oksana", IsNullable = false)]
    public class OrderUpdate
    {
        [XmlElement("orderId")]
        public string OrderId { get; set; }

        [XmlIgnore]
        public DateTimeOffset Created { get; set; }

        [XmlElement("created")]
        public string CreatedAsString
        {
            get
            {
                return Created.ToString("O");
            }

            set
            {
                Created = DateTimeOffset.Parse(value);
            }
        }

        [XmlIgnore]
        public DateTimeOffset Updated { get; set; }

        [XmlElement("updated")]
        public string UpdatedAsString
        {
            get
            {
                return Updated.ToString("O");
            }

            set
            {
                Updated = DateTimeOffset.Parse(value);
            }
        }

        [XmlElement("cardNumber")]
        public string CardNumber { get; set; }

        [XmlElement("phoneNumber")]
        public string PhoneNumber { get; set; }

        [XmlArray("rows")]
        [XmlArrayItem("row", IsNullable = false)]
        public Row[] Rows { get; set; }

        [XmlElement("deliveryPoint")]
        public DeliveryPointInfo DeliveryPoint { get; set; }

        [System.CodeDom.Compiler.GeneratedCode("xsd", "4.6.1055.0")]
        [System.Serializable()]
        [System.Diagnostics.DebuggerStepThrough()]
        [System.ComponentModel.DesignerCategory("code")]
        [XmlType(AnonymousType = true, Namespace = "http://www.olekstra.ru/schema/oksana")]
        public class Row
        {
            [XmlElement("rowId")]
            public string RowId { get; set; }

            [XmlElement("skuId")]
            public string SkuId { get; set; }

            [XmlElement("skuName")]
            public string SkuName { get; set; }

            [XmlElement("preorder")]
            public bool Preorder { get; set; }

            [XmlElement("quantity")]
            public int Quantity { get; set; }

            [XmlElement("price")]
            public decimal Price { get; set; }

            [XmlElement("discountPerItem")]
            public decimal DiscountPerItem { get; set; }

            [XmlElement("sumToPay")]
            public decimal SumToPay { get; set; }

            [XmlElement("status")]
            public OrderUpdateRowStatus Status { get; set; }

            [XmlElement("statusText")]
            public string StatusText { get; set; }

            [XmlIgnore]
            public DateTimeOffset? Purchased { get; set; }

            [XmlElement("purchased")]
            public string PurchasedAsString
            {
                get
                {
                    return Purchased?.ToString("O");
                }

                set
                {
                    Purchased = string.IsNullOrEmpty(value) ? (DateTimeOffset?)null : DateTimeOffset.Parse(value);
                }
            }
        }

        [System.CodeDom.Compiler.GeneratedCode("xsd", "4.6.1055.0")]
        [System.Serializable()]
        [System.Diagnostics.DebuggerStepThrough()]
        [System.ComponentModel.DesignerCategory("code")]
        [XmlType(AnonymousType = true, Namespace = "http://www.olekstra.ru/schema/oksana")]
        public class DeliveryPointInfo
        {
            [XmlElement("id")]
            public string Id { get; set; }

            [XmlElement("name")]
            public string Name { get; set; }

            [XmlElement("region")]
            public string Region { get; set; }

            [XmlElement("address")]
            public string Address { get; set; }

            [XmlElement("latitude")]
            public float Latitude { get; set; }

            [XmlElement("longitude")]
            public float Longitude { get; set; }

            [XmlElement("phones")]
            public string Phones { get; set; }

            [XmlElement("openHours")]
            public string OpenHours { get; set; }
        }
    }
}
