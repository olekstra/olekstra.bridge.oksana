﻿namespace Olekstra.Bridge.Oksana
{
    using System;
    using System.IO;
    using System.Text;
    using System.Xml;
    using System.Xml.Linq;
    using System.Xml.Schema;
    using System.Xml.Serialization;

    public static class SerializationHelper
    {
        private static readonly Lazy<XmlSchemaSet> Schemas = new Lazy<XmlSchemaSet>(() =>
        {
            var assembly = typeof(SerializationHelper).Assembly;
            var schemas = new XmlSchemaSet();

            using (var sr = assembly.GetManifestResourceStream("Olekstra.Bridge.Oksana.oksana.xsd"))
            {
                schemas.Add(null, XmlReader.Create(sr));
            }

            return schemas;
        });

        public static void SerializeXml(object value, Stream destination)
        {
            if (value == null)
            {
                throw new ArgumentNullException(nameof(value));
            }

            using (var sw = new StreamWriter(destination, new UTF8Encoding(false), 2048, true))
            {
                new XmlSerializer(value.GetType()).Serialize(sw, value);
            }
        }

        public static string SerializeXml(object value)
        {
            using (var ms = new MemoryStream())
            {
                SerializeXml(value, ms);
                ms.Position = 0;
                using (var sr = new StreamReader(ms))
                {
                    return sr.ReadToEnd();
                }
            }
        }

        public static TClass DeserializeXml<TClass>(Stream stream)
        {
            if (stream == null)
            {
                throw new ArgumentNullException(nameof(stream));
            }

            using (var sw = new StreamReader(stream, new UTF8Encoding(false), true, 2048, true))
            {
                var doc = XDocument.Load(sw);
                doc.Validate(Schemas.Value, null); // throws exception when validation error occurs
                var serializer = new XmlSerializer(typeof(TClass));
                return (TClass)serializer.Deserialize(doc.CreateReader());
            }
        }

        public static TClass DeserializeXml<TClass>(string text)
        {
            if (string.IsNullOrEmpty(text))
            {
                throw new ArgumentNullException(nameof(text));
            }

            using (var sw = new StringReader(text))
            {
                var doc = XDocument.Load(sw);
                doc.Validate(Schemas.Value, null); // throws exception when validation error occurs
                var serializer = new XmlSerializer(typeof(TClass));
                return (TClass)serializer.Deserialize(doc.CreateReader());
            }
        }
    }
}
