﻿namespace Olekstra.Bridge.Oksana
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml.Schema;
    using Microsoft.AspNetCore.Http;
    using Microsoft.Extensions.Logging;

    public class OksanaMiddleware
    {
        private const string ContentTypeXml = "application/xml";

        private readonly RequestDelegate next;

        private readonly IOksanaService service;

        private readonly ILogger logger;

        public OksanaMiddleware(RequestDelegate next, IOksanaService service, ILogger<OksanaMiddleware> logger)
        {
            this.next = next;
            this.service = service;
            this.logger = logger;
        }

        public async Task Invoke(HttpContext context)
        {
            var req = context.Request;
            var resp = context.Response;

            (bool authOk, string username) = await AuthorizeAsync(req);
            if (!authOk)
            {
                logger.LogInformation("Request interrupted: invalid authorization/authentication data");
                resp.StatusCode = (int)HttpStatusCode.Unauthorized;
                await resp.WriteAsync("Read Oksana protocol description and provide valid access credentials.");
                return;
            }

            if (req.Method != "POST")
            {
                resp.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                await resp.WriteAsync("Only POSTs are allowed.");
                return;
            }

            if (!string.Equals(req.ContentType, ContentTypeXml, StringComparison.OrdinalIgnoreCase))
            {
                resp.StatusCode = (int)HttpStatusCode.UnsupportedMediaType;
                await resp.WriteAsync($"Only '{ContentTypeXml}' is supported.");
                return;
            }

            switch (req.Path)
            {
                case "/cardStatus":
                    CardStatusRequest cardStatusRequest;
                    try
                    {
                        cardStatusRequest = SerializationHelper.DeserializeXml<CardStatusRequest>(req.Body);
                    }
                    catch (XmlSchemaValidationException ex)
                    {
                        resp.StatusCode = (int)HttpStatusCode.BadRequest;
                        await resp.WriteAsync(ex.Message);
                        return;
                    }

                    CardStatus cardStatus;
                    try
                    {
                        cardStatus = await service.ProcessCardStatusRequestAsync(username, cardStatusRequest);
                    }
                    catch (ArgumentException ex)
                    {
                        resp.StatusCode = (int)HttpStatusCode.BadRequest;
                        await resp.WriteAsync(ex.Message);
                        return;
                    }

                    resp.ContentType = ContentTypeXml;
                    SerializationHelper.SerializeXml(cardStatus, resp.Body);
                    break;

                case "/phoneCard":
                    PhoneCardRequest phoneCardRequest;
                    try
                    {
                        phoneCardRequest = SerializationHelper.DeserializeXml<PhoneCardRequest>(req.Body);
                    }
                    catch (XmlSchemaValidationException ex)
                    {
                        resp.StatusCode = (int)HttpStatusCode.BadRequest;
                        await resp.WriteAsync(ex.Message);
                        return;
                    }

                    PhoneCard phoneCard;
                    try
                    {
                        phoneCard = await service.ProcessPhoneCardRequestAsync(username, phoneCardRequest);
                    }
                    catch (ArgumentException ex)
                    {
                        resp.StatusCode = (int)HttpStatusCode.BadRequest;
                        await resp.WriteAsync(ex.Message);
                        return;
                    }

                    resp.ContentType = ContentTypeXml;
                    SerializationHelper.SerializeXml(phoneCard, resp.Body);
                    break;

                case "/orderUpdate":
                    OrderUpdate orderUpdate;
                    try
                    {
                        orderUpdate = SerializationHelper.DeserializeXml<OrderUpdate>(req.Body);
                        orderUpdate.Validate();
                    }
                    catch (XmlSchemaValidationException ex)
                    {
                        resp.StatusCode = (int)HttpStatusCode.BadRequest;
                        await resp.WriteAsync(ex.Message);
                        return;
                    }
                    catch (OrderUpdateValidationException ex)
                    {
                        resp.StatusCode = (int)HttpStatusCode.BadRequest;
                        await resp.WriteAsync(ex.Message);
                        return;
                    }

                    OrderUpdateResult orderUpdateResult;
                    try
                    {
                        await service.ProcessOrderUpdateAsync(username, orderUpdate);
                        orderUpdateResult = new OrderUpdateResult { Status = true };
                    }
                    catch (ArgumentException ex)
                    {
                        resp.StatusCode = (int)HttpStatusCode.BadRequest;
                        await resp.WriteAsync(ex.Message);
                        return;
                    }

                    resp.ContentType = ContentTypeXml;
                    SerializationHelper.SerializeXml(orderUpdateResult, resp.Body);
                    break;

                default:
                    resp.StatusCode = (int)HttpStatusCode.NotFound;
                    await resp.WriteAsync("Unknown URL, check docs");
                    break;
            }
        }

        public async Task<(bool success, string username)> AuthorizeAsync(HttpRequest request)
        {
            var authHeader = request.Headers["Authorization"].ToString();

            if (authHeader == null)
            {
                logger.LogDebug("No Auth header found");
                return (false, null);
            }

            if (!authHeader.StartsWith("Basic "))
            {
                logger.LogDebug("Auth header does not start with 'Basic ', invalid header");
                return (false, null);
            }

            var encodedUsernamePassword = authHeader.Split(new[] { ' ' }, 2, StringSplitOptions.RemoveEmptyEntries)[1]?.Trim();
            var decodedUsernamePassword = Encoding.UTF8.GetString(Convert.FromBase64String(encodedUsernamePassword));
            var usernameAndPassword = decodedUsernamePassword.Split(new[] { ':' }, 2);

            if (usernameAndPassword.Length != 2)
            {
                logger.LogDebug("Wrong Auth header content, can't split to username and password");
                return (false, null);
            }

            var authResult = await service.AuthorizeAsync(usernameAndPassword[0], usernameAndPassword[1]);
            logger.LogInformation($"Auth result for user '{usernameAndPassword[0]}': '{authResult}'");

            return (authResult, usernameAndPassword[0]);
        }
    }
}
