﻿namespace Olekstra.Bridge.Oksana
{
    using System;
    using System.Threading.Tasks;

    public interface IOksanaService
    {
        Task<bool> AuthorizeAsync(string username, string password);

        /// <summary>
        /// Process <see cref="CardStatusRequest"/> request.
        /// </summary>
        /// <param name="username">Username, successfully authorized using <see cref="AuthorizeAsync(string, string)"/> earlier.</param>
        /// <param name="cardStatusRequest">Request object.</param>
        /// <returns>Awaitable task with <see cref="CardStatus"/></returns>
        /// <exception cref="ArgumentException">CardStatusRequest is not valid (exception message will be written to client).</exception>
        Task<CardStatus> ProcessCardStatusRequestAsync(string username, CardStatusRequest cardStatusRequest);

        /// <summary>
        /// Process <see cref="PhoneCardRequest"/> request.
        /// </summary>
        /// <param name="username">Username, successfully authorized using <see cref="AuthorizeAsync(string, string)"/> earlier.</param>
        /// <param name="phoneCardRequest">Request object.</param>
        /// <returns>Awaitable task with <see cref="PhoneCard"/></returns>
        /// <exception cref="ArgumentException">PhoneCardRequest is not valid (exception message will be written to client).</exception>
        Task<PhoneCard> ProcessPhoneCardRequestAsync(string username, PhoneCardRequest phoneCardRequest);

        /// <summary>
        ///  Process <see cref="ProcessOrderUpdateAsync"/> request.
        /// </summary>
        /// <param name="username">Username, successfully authorized using <see cref="AuthorizeAsync(string, string)"/> earlier.</param>
        /// <param name="orderUpdate">Request object.</param>
        /// <returns>Awaitable task.</returns>
        /// <exception cref="ArgumentException">CardStatusRequest is not valid (exception message will be written to client).</exception>
        Task ProcessOrderUpdateAsync(string username, OrderUpdate orderUpdate);
    }
}
